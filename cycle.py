#!/usr/bin/env python3
from time import sleep
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
redLED = 23
GPIO.setup(redLED, GPIO.OUT)
GPIO.output(redLED, True)
GPIO.output(redLED, False)
yellowLED = 24
GPIO.setup(yellowLED, GPIO.OUT)
GPIO.output(yellowLED, True)
GPIO.output(yellowLED, False)
greenLED = 21
GPIO.setup(greenLED, GPIO.OUT)
GPIO.output(greenLED, True)
GPIO.output(greenLED, False)
LEDs = (redLED, yellowLED, greenLED)
for i in range(100):
    GPIO.output(LEDs[i % 3], True)
    sleep(2)
    GPIO.output(LEDs[i % 3], False)
