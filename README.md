# coopdoor

### defs.py

This file contains the pin definitions, see example-def.py for an example.

### Install

`cp example-defs.py defs.py` this file is gitignored by default

### usage

`./open.py` will open the door

`./close.py` will close the door

`./reset.py` will reset the linear actuator pins to low, and I run this upon boot in roots crontab like so

```
@reboot /root/coopdoor/reset.py
```

# notes

This is an adaptation of this [method](https://shiftautomation.com/controlling-a-linear-actuator-with-an-arduino-and-relays).
