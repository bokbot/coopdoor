#!/usr/bin/env python3
from time import sleep
import RPi.GPIO as GPIO
import defs
GPIO.setmode(GPIO.BCM)
GPIO.setup(defs.openActuatorRelay, GPIO.OUT)
GPIO.output(defs.openActuatorRelay, False)
GPIO.setup(defs.closeActuatorRelay, GPIO.OUT)
GPIO.output(defs.closeActuatorRelay, False)
GPIO.output(defs.closeActuatorRelay, True)
sleep(1)
GPIO.output(defs.closeActuatorRelay, False)
#GPIO.cleanup()
GPIO.cleanup(defs.openActuatorRelay)
GPIO.cleanup(defs.closeActuatorRelay)
